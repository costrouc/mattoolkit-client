#!/usr/bin/python3

"""
A Tool for Downloading CIF files from ICSD Database

Usage:

icsd = ICSD()
crystal = icsd.find_one(icsd_id=6084)
crystal.as_dict()
"""

import re
import json
import math
import functools

import requests
from bs4 import BeautifulSoup
from pymatgen.io.cif import CifParser
from pymatgen import Structure


class ICSDCrystal(object):
   def __init__(self, icsd_id, special_id, spacegroup, formula, structure_type, title, authors, journal, cif=None, structure=None):
      self.icsd_id = int(icsd_id)
      self.special_id = int(special_id)
      self.spacegroup = spacegroup
      self.formula = formula
      self.structure_type = structure_type
      self.publication = {
         'journal': journal,
         'authors': authors,
         'title': title
      }
      self._cif = cif
      self._structure = structure

   def write(self, filename):
      with open(filename, 'w') as f:
         f.write(self.cif)
      return filename

   @property
   def cif(self):
      if self._cif is None:
         self._cif = ICSD.download_cif(self.special_id)
      return self._cif

   @property
   def structure(self):
      if self._structure is None:
         parser = CifParser.from_string(self.cif)
         self._structure = parser.get_structures()[0]
      return self._structure

   def as_dict(self):
      return {
         '@module': self.__class__.__module__,
         '@class': self.__class__.__name__,
         'structure': self.structure.as_dict(),
         'formula': self.formula,
         'spacegroup': self.spacegroup,
         'publication': self.publication,
         'cif': self.cif,
         'icsd_id': self.icsd_id,
         'special_id': self.special_id,
         'structure_type': self.structure_type
      }

   @classmethod
   def from_dict(cls, d):
      return ICSDCrystal(
         icsd_id=d['icsd_id'],
         special_id=d['special_id'],
         spacegroup=d['spacegroup'],
         formula=d['formula'],
         structure_type = d['structure_type'],
         journal = d['publication']['journal'],
         title = d['publication']['title'],
         authors = d['publication']['authors'],
         cif=d.get('cif', None),
         structure=Structure.from_dict(d['structure']) if d.get('structure') else None
      )


def _new_icsd_session():
   session = requests.Session()
   session.headers.update({"User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0"})
   session.cookies.update({"CookieTest": "Enabled"})
   return session


class ICSD(object):
   _session = _new_icsd_session()

   @classmethod
   def find(cls, elements=None, spacegroup=None, system=None, icsd_id=None, limit=0):
      """ Search ICSD Database with given query

      params:
        elements: list of Strings (Composition) in ICSD Database
        spacegroup: int or string
        system: cubic, hexagonal, monoclinic, orthorombic, tetragonal, triclinic, or trigonal

      returns:
        list of ICSDCrystals
      """
      resp = cls._session.get('https://icsd.fiz-karlsruhe.de/search/basic.xhtml')
      soup = BeautifulSoup(resp.content, 'lxml')

      if elements and not isinstance(elements, (list, tuple)):
            raise Exception('elements must be of type list or tuple')

      if spacegroup:
         if isinstance(spacegroup, int) and (spacegroup < 1 or spacegroup > 230):
            raise Exception('spacegroup number out of range')

      if system and system not in ['cubic', 'hexagonal', 'monoclinic', 'orthorombic', 'tetragonal', 'triclinic', 'trigonal']:
         raise Exception("unknown crystal system %s" % system)

      data = {
         'content_form': 'content_form',
         'content_form:loginId': '',
         'content_form:password': '',
         'content_form:uiBibliographyAuthors:input_input': '',
         'content_form:uiBibliographyAuthors:input_hinput': '',
         'content_form:uiBibliographyPublicationYear:input:input': '',
         'content_form:uiBibliographyJournals:input_input': '',
         'content_form:uiBibliographyJournals:input_hinput': '',
         'content_form:uiBibliographyArticles:input_input': '',
         'content_form:uiBibliographyArticles:input_hinput': '',
         'content_form:uiChemistrySearchSumForm:input': ' '.join(elements) if elements else '',
         'content_form:uiChemistrySearchElCount:input:input': len(elements) if elements else '',
         'content_form:uiCellParameter:input': '',
         'content_form:uiCellVolume:input:input': '',
         'content_form:uiCellGlobalTolerance:input:input': '',
         'content_form:uiSymmetrySpaceGroupSymbol:input_input': spacegroup if isinstance(spacegroup, str) else '',
         'content_form:uiSymmetrySpaceGroupSymbol:input_hinput': spacegroup if isinstance(spacegroup, str) else '',
         'content_form:uiSymmetrySpaceGroupNumber:input:input': spacegroup if isinstance(spacegroup, int) else '',
         'content_form:uiymmetryCrystalSystem:input_input': system if system else '',
         'content_form:uiymmetryCrystalSystem:input_focus': '',
         'content_form:uiSymmetryCentering:input_input': '',
         'content_form:uiSymmetryCentering:input_focus': '',
         'content_form:uiPDFNumber:input_input': '',
         'content_form:uiPDFNumber:input_hinput': '',
         'content_form:uiExperimentalTemperature:input:input': '',
         'content_form:uiExperimentalTemperatureUnit:input_input': 'k',
         'content_form:uiExperimentalTemperatureUnit:input_focus': '',
         'content_form:uiCodeCollection:input:input': icsd_id if icsd_id else '',
         'content_form:uiExperimentalPressure:input:input': '',
         'content_form:uiExperimentalPressureUnit:input_input': 'mpa',
         'content_form:uiExperimentalPressureUnit:input_focus': '',
         'content_form:uiChemistryVisualSearchElCount': '',
         'content_form:uiChemistryVisualSearchUnitCoefficient_input': 'moles',
         'content_form:uiChemistryVisualSearchUnitCoefficient_focus': '',
         'content_form:btnRunQuery': '',
         'javax.faces.ViewState': soup.find('input', {'name': 'javax.faces.ViewState'}).get('value')
      }

      resp = cls._session.post('https://icsd.fiz-karlsruhe.de/search/basic.xhtml', data=data)

      # check search did not find any results
      if re.search(b'summary:"No results found for the given search criteria!"', resp.content):
         return None

      tableID = re.search(b"var listViewTableid='(.*)';", resp.content).group(1).decode('utf-8')
      pageID = int(re.search(b'var __RiaPageId = "(\d+)";', resp.content).group(1))
      numrows = int(re.search(b'# of Hits: (\d+)', resp.content).group(1))


      if limit and limit < numrows:
         numpages = math.ceil(limit / 10.0)
      else:
         numpages = math.ceil(numrows / 10.0)

      crystals = []
      for i in range(numpages):
         crystals += [cls._parse_row(row) for row in cls._get_page(tableID, pageID, i+1)]
      return crystals[:limit]

   @classmethod
   def find_one(cls, elements=None, spacegroup=None, system=None, icsd_id=None):
      result = cls.find(elements, spacegroup, system, icsd_id, limit=1)
      if result is None:
         return None
      return result[0]

   @staticmethod
   def _parse_row(row):
      match = re.search('onclick="exportCifFile\((\d+),(\d+)\)"', row[10])
      special_id, icsd_id = match.groups()

      return ICSDCrystal(
         icsd_id=int(icsd_id),
         special_id=int(special_id),
         spacegroup=BeautifulSoup(row[2], 'lxml').text,
         formula=BeautifulSoup(row[3], 'lxml').text,
         structure_type=BeautifulSoup(row[4], 'lxml').text,
         title=BeautifulSoup(row[5], 'lxml').text,
         authors=BeautifulSoup(row[6], 'lxml').text.split(';'),
         journal=BeautifulSoup(row[7], 'lxml').text,
      )

   @classmethod
   @functools.lru_cache(maxsize=64)
   def download_cif(cls, special_id):
      resp = cls._session.get(
         'https://icsd.fiz-karlsruhe.de/viscalc/jsp/jsp/downloadFile.action',
         params={
            'submittingParameter': 'Single CIF File',
            'saveSingleIDNUM': special_id,
            'userAssignedFileName': 'pyICSDDownloader'
         })
      return resp.content.decode('utf-8')

   @classmethod
   def _get_page(cls, tableID, pageID, page):
      columns = {
         "SweetDevRiaComponent_52371": {
            "id": "SweetDevRiaComponent_52371", "header": "Coll. Code", "position": 0, "initialPosition": 0, "visible": True, "size": 70, "ascendant": True, "sortOrder": 1, "sortable": True, "resizable": True, "hideable": True
         },
         "SweetDevRiaComponent_52372": {
            "id": "SweetDevRiaComponent_52372", "header": "HMS", "position": 1, "initialPosition": 1, "visible": True, "size": 55, "ascendant": None, "sortOrder": None, "sortable": True, "resizable": True, "hideable": True
         },
         "SweetDevRiaComponent_52373": {
            "id": "SweetDevRiaComponent_52373", "header": "Struct. Form.", "position": 2, "initialPosition": 2, "visible": True, "size": 85, "ascendant": None, "sortOrder": None, "sortable": True, "resizable": True, "hideable": True
         },
         "SweetDevRiaComponent_52374": {
            "id": "SweetDevRiaComponent_52374", "header": "Struct. Type", "position": 3, "initialPosition": 3, "visible": True, "size": 80, "ascendant": None, "sortOrder": None, "sortable": True, "resizable": True, "hideable": True
         },
         "SweetDevRiaComponent_52375": {
            "id": "SweetDevRiaComponent_52375", "header": "Title", "position": 4, "initialPosition": 4, "visible": True, "size": 120, "ascendant": None, "sortOrder": None, "sortable": True, "resizable": True, "hideable": True
         },
         "SweetDevRiaComponent_52376": {
            "id": "SweetDevRiaComponent_52376", "header": "Authors", "position": 5, "initialPosition": 5, "visible": True, "size": 90, "ascendant": None, "sortOrder": None, "sortable": True, "resizable": True, "hideable": True
         },
         "SweetDevRiaComponent_52377": {
            "id": "SweetDevRiaComponent_52377", "header": "Reference", "position": 6, "initialPosition": 6, "visible": True, "size": 125, "ascendant": None, "sortOrder": None, "sortable": True, "resizable": True, "hideable": True
         },
         "SweetDevRiaComponent_52378": {
            "id": "SweetDevRiaComponent_52378", "header": "IDNUM", "position": 7, "initialPosition": 7, "visible": False, "size": 0, "ascendant": None, "sortOrder": None, "sortable": False, "resizable": False, "hideable": True
         },
         "SweetDevRiaComponent_52379": {
            "id": "SweetDevRiaComponent_52379", "header": "<img align='middle' src='/viscalc/resources/stern-schwarz.gif'><span style='display:none'>Quality<\/span>", "position": 8, "initialPosition": 8, "visible": True, "size": 30, "ascendant": False, "sortOrder": 0, "sortable": True, "resizable": True, "hideable": True
         },
         "SweetDevRiaComponent_52380": {
            "id": "SweetDevRiaComponent_52380", "header": "<div style='background:#CBDFF1; height:28px;'/><span style='color:#CBDFF1'>Save<\/span>", "position": 9, "initialPosition": 9, "visible": True, "size": 25, "ascendant": None, "sortOrder": None, "sortable": False, "resizable": False, "hideable": True
         }
      }

      data = [{
         "type": "updateModel",
         "idSrc": tableID,
         "params": {
            "synchroCall": True,
            "gridId": tableID,
            "columns": columns,
            "width": 750,
            "height": None,
            "draggableColumns": True,
            "resizable": False,
            "selectionMode": 1,
            "selection": None,
            "checkedRows": [],
            "actualPage": page
         },
         "sendServer": False,
         "synchroCall": True,
         "gridId": tableID,
         "columns": columns,
         "width": 750,
         "height": None,
         "draggableColumns": True,
         "resizable": False,
         "selectionMode": 1,
         "selection": None,
         "checkedRows": [],
         "actualPage": page
      },{
         "type": "pagin",
         "idSrc": tableID,
         "params": {
            "gridId": tableID,
            "actualPage": page
         },
         "sendServer": False,
         "gridId": tableID,
         "actualPage": page
      }]

      resp = cls._session.post(
         'https://icsd.fiz-karlsruhe.de/viscalc/RiaController',
         data={
            '__RiaPageId': pageID,
            'eventXml': json.dumps(data)
         })

      return resp.json()[0][0]['params']['data']
